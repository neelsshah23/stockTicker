import {Component, OnInit} from '@angular/core';
import {CompanyService} from "./services/company.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [CompanyService]
})
export class AppComponent implements OnInit{
  title = 'StockTicker';
  showLoader = false;
  selectedSymbol = 'all';
  companyDataList = [];


  SYMBOLS = ['AAPL','MSFT','IBN','V','HDB','PYPL','TSLA','FB','AXP','KO','BABA','VOD'];

  companyData = [];

  constructor (private companyService: CompanyService) {

  }

  ngOnInit() {
    this.getCompanyDetails();
  }

  getCompanyDetails() {
    this.showLoader = true;

    for (let i = 0; i < this.SYMBOLS.length; i++) {

      const data = {
        symbol: this.SYMBOLS[i]
      };

      this.companyService.getCompanyDetails(data, (companyDetails) => {

        this.companyService.getCompanyTops(data , (companyTrades) => {

          companyDetails['companyTrades'] = companyTrades[data.symbol][0];


          this.companyService.getCompanyLogo(data , (companyLogoData) => {
            companyDetails['logo'] = companyLogoData.url;

            this.companyData.push(companyDetails);
            this.companyDataList = this.companyData;
            this.showLoader = false;


          })

        })



      })
    }
  }

  updateCompanyList(data) {
    if ( data === 'all') {
      this.companyDataList = this.companyData;
    } else {

      for (let i = 0; i < this.companyData.length; i++) {
        if (this.companyData[i].symbol === data) {
          this.companyDataList = [this.companyData[i]];
        }
      }

    }
  }


}
