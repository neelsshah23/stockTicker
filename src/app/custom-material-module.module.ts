import { NgModule } from '@angular/core';
import {
  MdButtonModule,
  MdCheckboxModule,
  MdDatepickerModule,
  MdInputModule,
  MdSelectModule,
  MdMenuModule,
  MdToolbarModule,
  MdCardModule,
  MdTabsModule,
  MdProgressSpinnerModule,
  MdDialogModule,
  MdTooltipModule,
  MdSnackBarModule,
  MdTableModule,
  MdPaginatorModule,
  MdNativeDateModule,
  MdListModule,
  MdProgressBarModule,
  MdRadioModule
} from '@angular/material';

const moduleList = [
  MdButtonModule,
  MdCheckboxModule,
  MdDatepickerModule,
  MdInputModule,
  MdSelectModule,
  MdMenuModule,
  MdToolbarModule,
  MdCardModule,
  MdTabsModule,
  MdProgressSpinnerModule,
  MdDialogModule,
  MdTooltipModule,
  MdSnackBarModule,
  MdTableModule,
  MdPaginatorModule,
  MdNativeDateModule,
  MdListModule,
  MdProgressBarModule,
  MdRadioModule
];

@NgModule({
  imports: moduleList,
  exports: moduleList
})
export class CustomMaterialModuleModule { }
