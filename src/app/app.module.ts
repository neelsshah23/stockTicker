import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import {Routes, RouterModule, Router, NavigationStart, Event as NavigationEvent, NavigationEnd} from '@angular/router';



import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import { CustomMaterialModuleModule } from './custom-material-module.module';

import { AppComponent } from './app.component';
import {ApiCallService} from './services/http.service';
import {CommonComponent} from './common/common.component';
import {CompanyService} from './services/company.service';

@NgModule({
  declarations: [
    AppComponent,
    CommonComponent
  ],
  imports: [
    BrowserModule,
    CustomMaterialModuleModule,
    FormsModule,
    HttpModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      [
        { path: '', redirectTo: '/', pathMatch: 'full' }
      ],
      {useHash: true})
  ],
  providers: [ApiCallService, CompanyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
