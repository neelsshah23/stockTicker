import { Injectable } from '@angular/core'
import { Http, URLSearchParams, Response, Headers } from '@angular/http'
import { RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class ApiCallService {

  constructor(private http: Http, private router: Router) { }

  createAuthorizationHeader(headers: Headers) {
    headers.append('authorization', window.localStorage.getItem('_t'));
  }
  handleResponse(response) {
    if (response.status_code === 200 || response.status_code === 201) {
      return response;
    } else {
      return null;
    }
  }

  handleErrorResponse(error) {
    if (error.statusCode === 401) {
      console.log('here');
      // @todo: please code for tooltil and error message here
      this.router.navigateByUrl('login');
    } else {
      return Observable.throw(error.error || 'Server Error');
    }
  }

  callGetApi(apiurl: string, params?: URLSearchParams) {
    const header = new Headers();
    this.createAuthorizationHeader(header);
    return this.http.get(apiurl, {
      search: params,
      headers: header
    })
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return this.handleErrorResponse(error.json());
        }
      );
  }

  callGetApiNoAuth(apiurl: string) {

    return this.http.get(apiurl, {
    })
      .map(
        (response: Response) => {
          return response.json();
        }
      )
      .catch(
        (error: Response) => {
          return this.handleErrorResponse(error.json());
        }
      );
  }

  callPostApi(apiurl: string, body: any, options?) {
    return this.http.post(apiurl, body, options).map(
      (response: Response) => {
        return this.handleResponse(response.json());
      }
    ).catch(
      (error: Response) => Observable.throw(error.json())
    );
  }

  callPutApi(apiurl: string, body: any, params?: URLSearchParams) {
    return this.http.post(apiurl, body, {
      search: params
    }).map(
      (response: Response) => {
        return this.handleResponse(response.json());
      }
    ).catch(
      (error: Response) => Observable.throw(error.json().error || 'Server Error')
    );
  }

  callDeleteApi(apiurl: string, params?: URLSearchParams) {
    return this.http.post(apiurl, {
      search: params
    }).map(
      (response: Response) => {
        return this.handleResponse(response.json());
      }
    ).catch(
      (error: Response) => Observable.throw(error.json().error || 'Server Error')
    );
  }

}
