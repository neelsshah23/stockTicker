import { Injectable } from '@angular/core';
import { ApiCallService } from './http.service';
import { Globalurl } from '../common/globalurl';



@Injectable()
export class CompanyService {


  constructor(private apiCall: ApiCallService) { }

  getCompanyDetails(data, callback) {

    this.apiCall.callGetApi(`${Globalurl.COMPANY}/${data.symbol}/company`).subscribe(
      (Response) => {
        callback(Response);
      },
      (error) => {
        console.log(error);
      }
    );
  };

  getCompanyLogo(data, callback) {

    this.apiCall.callGetApi(`${Globalurl.COMPANY}/${data.symbol}/logo`).subscribe(
      (Response) => {
        callback(Response);
      },
      (error) => {
        console.log(error);
      }
    );
  };

  getCompanyTops(data, callback) {

    this.apiCall.callGetApi(`${Globalurl.COMPANY_TRADES}${data.symbol.toLowerCase()}`).subscribe(
      (Response) => {
        callback(Response);
      },
      (error) => {
        console.log(error);
      }
    );
  };


}
