import { StockTickerPage } from './app.po';

describe('stock-ticker App', () => {
  let page: StockTickerPage;

  beforeEach(() => {
    page = new StockTickerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
